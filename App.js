import React, { Component } from 'react';
import Index from './src/index';

class App extends Component {
  render() {
    return (
      <Index />
    );
  }
}

export default App;
