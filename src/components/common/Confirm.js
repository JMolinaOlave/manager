import React, { Component } from 'react';
import { Text, View, Modal } from 'react-native';
import { CardSection } from './CardSection';
import { Button } from './Button';

class Confirm extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const { containerStyle, cardSectionStyle, textStyle } = style;
    const { children, onAccept, onDecline } = this.props;

    return (
      <Modal
        visible={this.props.visible}
        transparent
        animationType="slide"
        onRequestClose={() => {}}
      >
        <View style={ containerStyle }>
          <CardSection style={ cardSectionStyle }>
            <Text style={ textStyle }>{ children }</Text>
          </CardSection>

          <CardSection>
            <Button onPress={ onAccept }>Si</Button>
            <Button onPress={ onDecline }>No</Button>
          </CardSection>
        </View>
      </Modal>
    );
  }
}

const style = {
  cardSectionStyle: {
    justifyContent: 'center',
  },
  textStyle: {
    flex: 1,
    fontSize: 18,
    textAlign: 'center',
    lineHeight: 40,
  },
  containerStyle: {
    backgroundColor: 'rgba(0, 0, 0, 0.75)',
    position: 'relative',
    flex: 1,
    justifyContent: 'center',
  },
};

export { Confirm };
