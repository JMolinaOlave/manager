/* @flow */

import React, { Component } from 'react';
import { View, Text, Picker } from 'react-native';
import { connect } from 'react-redux';
import { employeeUpdate } from '../actions';
import { CardSection, Input } from './common';

class EmployeeForm extends Component {
  render() {
    return (
      <View>
        <CardSection>
          <Input
            label="Nombre"
            placeholder="Ricardo Canitrot"
            value={ this.props.name }
            onChangeText={
              value => this.props.employeeUpdate({ prop: 'name', value })
            }
          />
        </CardSection>

        <CardSection>
          <Input
            label="Fono"
            placeholder="9 123 456 78"
            value={ this.props.phone }
            onChangeText={
              value => this.props.employeeUpdate({ prop: 'phone', value })
            }
          />
        </CardSection>

        <CardSection style={{ flexDirection: 'column' }}>
          <Text style={ styles.pickerTextStyle }>Turno</Text>
          <Picker
            selectedValue={ this.props.shift }
            onValueChange={
              value => this.props.employeeUpdate({ prop: 'shift', value })
            }
          >
            <Picker.Item label="Lunes" value="Lunes"/>
            <Picker.Item label="Martes" value="Martes"/>
            <Picker.Item label="Miércoles" value="Miércoles"/>
            <Picker.Item label="Jueves" value="Jueves"/>
            <Picker.Item label="Viernes" value="Viernes"/>
          </Picker>
        </CardSection>
      </View>
    );
  }
}


const styles = {
  pickerTextStyle: {
    fontSize: 18,
    paddingLeft: 20,
    paddingTop: 10,
  },
};

const mapStateToProps = state => {
  const { name, phone, shift } = state.employeeForm;
  return { name, phone, shift };
};

export default connect(mapStateToProps, { employeeUpdate })(EmployeeForm);
